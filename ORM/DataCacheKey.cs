﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.DataMapping
{
    internal class DataCacheKey
    {
        public string Query { get; }
        public (string, object)[] Parameters { get; }
        internal DataCacheKey(string query, (string, object)[] parameters)
        {
            this.Query = query;
            this.Parameters = parameters;
        }

        /*public override bool Equals(object? obj)
        {
            if (obj is DataCacheKey key)
            {

                return Query.Equals(key.Query) && Parameters.SequenceEqual(key.Parameters);
            }

            return false;
        }*/


        protected bool Equals(DataCacheKey other)
        {
            return Query == other.Query && Parameters.Equals(other.Parameters);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Query, Parameters);
        }
    }
}
