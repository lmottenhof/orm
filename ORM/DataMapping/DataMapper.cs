﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace ORM
{
    public abstract class DataMapper<TDest, TSource> where TDest : new()
    {
        #region Fields

        protected Type DestType = typeof(TDest);
        protected Type SourceType = typeof(TSource);


        #endregion Fields
        protected DataMapper()
        {

        }

        public abstract TDest Map(TSource src);
        

        #region Methods

        protected virtual IEnumerable<KeyValuePair<PropertyInfo, PropertyInfo>> GetMatchingProperties()
        {
            Dictionary<PropertyInfo, PropertyInfo> matches = new Dictionary<PropertyInfo, PropertyInfo>();
            IEnumerable<PropertyInfo> getters = GetGetters(SourceType);
            
            foreach (PropertyInfo setter in GetSetters(this.DestType))
            {
                PropertyInfo? getter = getters.SingleOrDefault(g => g.Name == setter.Name);

                if(getter == null) throw new MissingMemberException(setter.DeclaringType?.Name, setter.Name);
                matches.Add(setter, getter);
            }

            return matches;
        }

        protected static IEnumerable<PropertyInfo> GetGetters(Type type)
        {
            return type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.GetMethod != null);
        }

        protected static IEnumerable<PropertyInfo> GetSetters(Type type)
        {
            return type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.SetMethod != null);
        }

        protected static FieldInfo GetBackingField(PropertyInfo property)
        {
            FieldInfo? field = property.DeclaringType.GetField($"<{property}>k_BackingField");
            if (field == null)  throw new MissingMemberException(property.DeclaringType.Name, property.Name);

            return field;
        }
        #endregion Methods
    }
}
