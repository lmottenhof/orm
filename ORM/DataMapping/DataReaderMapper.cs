﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Timers;
using Sigil;
using Label = System.Reflection.Emit.Label;

namespace ORM
{
    public class DataReaderMapper<T> : DataMapper<T, IDataReader>, IMassDataMapper<T, IDataReader> where T : new()
    {
        private readonly Type outType = typeof(T);
        private readonly Type dataReaderType = typeof(IDataReader);
        private readonly Type dataRecordType = typeof(IDataRecord);
        
        private static Dictionary<Type, Func<IDataReader, T>> mappers = new Dictionary<Type, Func<IDataReader, T>>();
        
        private DataTable schema;
        readonly FieldInfo dbNullField = typeof(DBNull).GetField("Value", BindingFlags.Static | BindingFlags.Public);
        private  MethodInfo dataReaderValueMethod;
        private  MethodInfo dataReaderOrdinalMethod;
        
        private Func<IDataReader, T> CreateMapFunc()
        {
            if (DataReaderMapper<T>.mappers.ContainsKey(outType)) return mappers[outType];
            Stopwatch watch = new Stopwatch();
            watch.Start();
            dataReaderValueMethod =
                dataRecordType.GetMethod("GetValue", BindingFlags.Instance | BindingFlags.Public);
            dataReaderOrdinalMethod =
                dataRecordType.GetMethod("GetOrdinal", BindingFlags.Instance | BindingFlags.Public);
            
            DynamicMethod dynamicMethod = new DynamicMethod("MapFunc", outType, new[] {dataReaderType}, true);
            ConstructorInfo defaultConstructor =
                typeof(T).GetConstructor(new Type[] { }) ?? throw new NoSuchMethodException();
            ILGenerator generator = dynamicMethod.GetILGenerator();
            generator.Emit(OpCodes.Newobj, defaultConstructor);
            
            PropertyInfo[] properties = outType.GetProperties(
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Label nextLabel = generator.DefineLabel();
            foreach (PropertyInfo property in properties)
            {
                //Each set of assignment operation will point to the next set of operations, will will actually be generated next
                nextLabel = GenerateAssignmentInstruction(generator, nextLabel, property);
            }

            generator.MarkLabel(nextLabel);
            generator.Emit(OpCodes.Ret);
            
            Func<IDataReader, T> mapFunc = (Func<IDataReader, T>) dynamicMethod.CreateDelegate(typeof(Func<IDataReader, T>));
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds);
            DataReaderMapper<T>.mappers.Add(outType, mapFunc);
            return mapFunc;
        }

        private Label GenerateAssignmentInstruction(ILGenerator generator, Label labelToGenerate, PropertyInfo property)
        {
           //Comments represent the stack
            generator.MarkLabel(labelToGenerate);
            generator.Emit(OpCodes.Dup); //outType, outType
            generator.Emit(OpCodes.Ldarg_0);// outType, outType, reader
            generator.Emit(OpCodes.Dup);// outType,outType, reader, reader
            generator.Emit(OpCodes.Ldstr, property.Name);// outType, outType, reader, reader, propertyname
            generator.Emit(OpCodes.Callvirt, dataReaderOrdinalMethod);// outType, outType, reader, int
            generator.Emit(OpCodes.Callvirt, dataReaderValueMethod);// outType, outType, value
            generator.Emit(OpCodes.Dup);// outType, outType, value, value
            generator.Emit(OpCodes.Ldsfld, dbNullField);// outType, outType, value, value, dbnull
            //Check if result from reader is DBNull. Jump to unboxing and assignment if not equal.
            Label notNullLabel = generator.DefineLabel();
            Label next = generator.DefineLabel();
            generator.Emit(OpCodes.Bne_Un_S, notNullLabel);// outType, outType, value
            generator.Emit(OpCodes.Pop);// outType, outType
            generator.Emit(OpCodes.Pop);//outType
            generator.Emit(OpCodes.Br_S, next);
            generator.MarkLabel(notNullLabel);
            generator.Emit(OpCodes.Unbox_Any, property.PropertyType);// outType, outType, unboxed value
            generator.Emit(OpCodes.Callvirt, property.SetMethod);// outType
            generator.Emit(OpCodes.Br_S, next);
            return next;
        }

        public override T Map(IDataReader src)
        {
            Func<IDataReader, T> map = this.CreateMapFunc();
            return map(src);
        }

        public IEnumerable<T> MapAll(IDataReader src)
        {
            Func<IDataReader, T> map = this.CreateMapFunc();
            while (src.Read())
            {
                yield return map(src);
            }
        }
    }
}