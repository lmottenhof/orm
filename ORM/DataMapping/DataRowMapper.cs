﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ORM
{
    public class DataRowMapper<TDest> : DataMapper<TDest, DataRow>, IMassDataMapper<TDest, DataTable> where TDest : new()
    {
        public DataRowMapper()
        {
            //TODO
        }

        public override TDest Map(DataRow src)
        {
            //TODO
            throw new NotImplementedException();
        }

        public IEnumerable<TDest> MapAll(DataTable src)
        {
            foreach (DataRow row in src.Rows)
            {
                yield return Map(row);
            }
        }
    }
}
