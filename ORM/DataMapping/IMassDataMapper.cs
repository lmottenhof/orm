﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM
{
    public interface IMassDataMapper<out TDest, in TSource>
    {
        IEnumerable<TDest> MapAll(TSource src);
    }
}


