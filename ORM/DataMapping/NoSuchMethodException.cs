﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM
{
    /// <summary>
    /// This exception is throw when object relational mapping is not possible due to a missing method
    /// </summary>
    public class NoSuchMethodException : Exception
    {

    }
}
