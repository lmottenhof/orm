﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ORM
{
    public class ObjectMapper<TDest,TSource> : DataMapper<TDest, TSource>, IMassDataMapper<TDest, IEnumerable<TSource>> where TDest : new()
    {
        public override TDest Map(TSource src)
        {
            //Simple implementation, very slow
            //TODO
            TDest dest = new TDest();
            foreach ((PropertyInfo destProp, PropertyInfo sourceProp) in GetMatchingProperties())
            {
                destProp.SetMethod.Invoke(dest, new [] { sourceProp.GetMethod.Invoke(src, null)});
            }

            return dest;
        }

        public IEnumerable<TDest> MapAll(IEnumerable<TSource> src)
        {
            return src.Select(Map);
        }
    }
}
