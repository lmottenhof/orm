﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using ORM.DataMapping;

namespace ORM
{
    public class DatabaseContext
    {
        private readonly IDbConnection conn;
        private readonly IDbCommand command;

        public IMemoryCache? cache;

        public DatabaseContext(IDbConnection conn)
        {
            this.conn = conn;
            this.command = conn.CreateCommand();
        }

        public DatabaseContext(IDbConnection conn, IMemoryCache cache) : this(conn)
        {
            this.cache = cache;
        }

        public IEnumerable<TDest> Query<TDest>(string query, params (string name, object value)[] parameters)
            where TDest : new() =>
            Query(query, new DataReaderMapper<TDest>(), CommandType.Text, parameters);

        public IEnumerable<TDest> Query<TDest>(string query, IMassDataMapper<TDest, IDataReader> mapper,
            params (string name, object value)[] parameters) where TDest : new() =>
            Query(query, mapper, CommandType.Text, parameters);

        public IEnumerable<TDest> Query<TDest>(string query, IMassDataMapper<TDest, IDataReader> mapper,
            CommandType commandType = CommandType.Text, params (string name, object value)[] parameters)
            where TDest : new()
        {
            if (this.cache != null &&
                this.cache.TryGetValue(new DataCacheKey(query, parameters), out IEnumerable<TDest> data))
            {
                return data;
            }

            lock (command)
            {
                this.PrepareCommand(query, commandType, parameters);
                try
                {
                    lock (conn)
                    {
                        conn.Open();

                        {
                            IDataReader reader = this.command.ExecuteReader();
                            return mapper.MapAll(reader).ToArray();
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public int Execute(string query, params (string name, object value)[] parameters) =>
            Execute(query, CommandType.Text, parameters);


        public int Execute(string query, CommandType commandType = CommandType.Text,
            params (string name, object value)[] parameters)
        {
            lock (command)
            {
                PrepareCommand(query, commandType, parameters);
                try
                {
                    lock (conn)
                    {
                        conn.Open();
                        return command.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public object Scalar(string query, params (string name, object value)[] parameters) =>
            Scalar(query, CommandType.Text, parameters);

        public object Scalar(string query, CommandType commandType = CommandType.Text,
            params (string name, object value)[] parameters)
        {
            try
            {
                lock (command)
                {
                    PrepareCommand(query, commandType, parameters);
                    lock (conn)
                    {
                        conn.Open();
                        return command.ExecuteScalar();
                    }
                }
            }
            finally
            {
                conn.Close();
            }
        }

        private void PrepareCommand(string query, CommandType commandType,
            params (string name, object value)[] parameters)
        {
            this.command.Parameters.Clear();
            this.command.CommandText = query;
            this.command.CommandType = commandType;
            foreach ((string name, object value) in parameters)
            {
                IDbDataParameter param = command.CreateParameter();
                param.ParameterName = name;
                param.Value = value;
                command.Parameters.Add(param);
            }
        }
    }
}