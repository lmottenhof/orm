﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Repositories
{
    public interface IReadableRepository<T>
    {
        IEnumerable<T> Read();

    }
}
