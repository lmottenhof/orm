﻿using System;
using System.Collections.Generic;
using System.Text;
using ORM.Repositories;

namespace ORM
{
    public interface IRepository<T> : IReadableRepository<T>, IWritableRepository<T>
    {
    }
}
