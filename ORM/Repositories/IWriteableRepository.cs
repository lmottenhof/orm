﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORM.Repositories
{
    public interface IWritableRepository<T>
    {
        public int Update(T obj);
        public int Delete(T obj);
        public int Insert(T obj);
    }
}
