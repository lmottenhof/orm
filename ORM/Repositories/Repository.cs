﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Microsoft.Extensions.Caching.Memory;
using ORM.Repositories;

namespace ORM
{
    public abstract class Repository<T> : IRepository<T> where T : class, new()
    {
        protected DatabaseContext context;
        private string table;
        private DataTable schema;
        private IMemoryCache cache;

        public IMassDataMapper<T, IDataReader> DataMapper { get; set; } = new DataReaderMapper<T>();
        
        public Repository(RepositorySpecification repoSpec)
        {
            this.cache = repoSpec.Cache;
            this.context = repoSpec.Context;
            this.table = repoSpec.Table;
            this.cache = repoSpec.Cache;
        }
        
        public virtual int Insert(T obj)
        {
            
            //TODO
            throw new NotImplementedException();
        }
        public virtual IEnumerable<T> Read()
        {
            return this.context.Query($"SELECT * FROM {table}", this.DataMapper);
        }

        public virtual int Update(T obj)
        {
            //TODO
            throw new NotImplementedException();
        }

        public virtual int Delete(T obj)
        {
            //TODO
            throw new NotImplementedException();
        }
    }
}
