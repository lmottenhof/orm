using System.Data;
using System.Data.Common;
using Microsoft.Extensions.Caching.Memory;

namespace ORM.Repositories
{
    public class RepositorySpecification
    {
        public RepositorySpecification(DbConnection conn, IMemoryCache? cache)
        {
            this.Cache = cache;
            conn.Open();
            try
            {
                this.Schema = conn.GetSchema();
            }
            finally
            {
                conn.Close();
            }
           
            this.Context = new DatabaseContext(conn, cache);
        }
        
        internal  DatabaseContext Context { get; set; }
        internal IMemoryCache Cache { get; set; }
        internal DataTable Schema { get; set; }
        public string Table { get; set; }
    }
}