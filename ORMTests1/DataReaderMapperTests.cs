﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ORM;

namespace ORMTests
{
    [TestClass]
    public class DataReaderMapperTests
    {
        private DataTable dt;
        [TestInitialize]
        public void Setup()
        {
            dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Value");
            dt.Columns["Name"].DataType = typeof(string);
            dt.Columns["Value"].DataType = typeof(string);

            for (int i = 0; i < 5; ++i)
            {
                dt.Rows.Add("Name" + i, "Value" + i);
            }
        }

        [TestMethod]
        public void MapTest()
        {
            DataReaderMapper<TestData> dataMapper = new DataReaderMapper<TestData>();
            IDataReader reader = dt.CreateDataReader();
            reader.Read();
            TestData data = dataMapper.Map(reader);

            Assert.AreEqual(dt.Rows[0].Field<string>("Name"), data.Name);
            Assert.AreEqual(dt.Rows[0].Field<string>("Value"), data.Value);
        }

        [TestMethod]
        public void MapAllTest()
        {
            DataReaderMapper<TestData> dataMapper = new DataReaderMapper<TestData>();
            IDataReader reader = dt.CreateDataReader();
            List<TestData> data = dataMapper.MapAll(reader).ToList();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Assert.AreEqual(dt.Rows[i].Field<string>("Name"), data[i].Name);
                Assert.AreEqual(dt.Rows[i].Field<string>("Value"), data[i].Value);
            }
           
        }
    }
}