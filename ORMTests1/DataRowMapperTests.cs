﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ORM;

namespace ORMTests
{
    [TestClass]
    public class DataRowMapperTests
    {
        private DataTable dt;
        [TestInitialize]
        public void Setup()
        {
            dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Value");
            dt.Columns["Name"].DataType = typeof(string);
            dt.Columns["Value"].DataType = typeof(string);

            for (int i = 0; i < 5; ++i)
            {
                dt.Rows.Add("Name" + i, "Value" + i);
            }
        }


        [TestMethod]
        public void MapTest()
        {
            DataRowMapper<TestData> dataMapper = new DataRowMapper<TestData>();
            TestData data = dataMapper.Map(dt.Rows[0]);

            Assert.AreEqual(dt.Rows[0].Field<string>("Name"),data.Name);
            Assert.AreEqual(dt.Rows[0].Field<string>("Value"), data.Value);
        }

        [TestMethod]
        public void MapAllTest()
        {
            DataRowMapper<TestData> dataMapper = new DataRowMapper<TestData>();
            List<TestData> data = dataMapper.MapAll(dt).ToList();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Assert.AreEqual(dt.Rows[i].Field<string>("Name"), data[i].Name);
                Assert.AreEqual(dt.Rows[i].Field<string>("Value"), data[i].Value);
            }

        }
    }
}