﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ORM;

namespace ORMTests
{
    [TestClass]
    public class ObjectMapperTests
    {
        [TestMethod]
        public void MapTest()
        {
            ObjectMapper<TestData, TestObject> mapper = new ObjectMapper<TestData, TestObject>();
            TestObject testObject = new TestObject { Name = "Name", Value = "Value" };
            TestData data = mapper.Map(testObject);
            Assert.AreEqual(testObject.Name, data.Name);
            Assert.AreEqual(testObject.Value, data.Value);
        }

        [TestMethod]
        public void MapAllTest()
        {
            ObjectMapper<TestData, TestObject> mapper = new ObjectMapper<TestData, TestObject>();

            List<TestObject> objects = new List<TestObject>
            {
                new TestObject {Name = "Name", Value = "Value"},
                new TestObject {Name = "Name2", Value = "Value2"}
            };

            List<TestData> data = mapper.MapAll(objects).ToList();
            for (int i = 0; i < objects.Count; i++)
            {
                Assert.AreEqual(objects[i].Name, data[i].Name);
                Assert.AreEqual(objects[i].Value, data[i].Value);
            }
        }
    }
}